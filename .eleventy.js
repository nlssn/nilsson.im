const { EleventyHtmlBasePlugin } = require('@11ty/eleventy')

module.exports = (eleventyConfig) => {
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin)
  eleventyConfig.addWatchTarget('./src/assets/scss/')
  eleventyConfig.addPassthroughCopy('./src/assets/img')

  return {
    dir: {
      input: 'src',
      output: 'dist',
      layouts: '_layouts',
    },
  }
}
